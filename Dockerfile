FROM node:10-alpine

RUN npm install -g nodemon
RUN npm install -g typescript

# Create app directory
WORKDIR /usr/src/app
